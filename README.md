# Kemperverse frontend

Link to live server: https://kemperverse.systems/

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

---

## Tools and Configuration
| Features           | Enabled | Additional Info                                                                                                                                                              |
| ------------------ | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Babel              | No      | A javascript compiler, makes code backwards compatible, complicates build and is not necessary for our purposes
| TypeScript         | Yes     | Static typing in javascript, prevents many common errors                                                                                                                     |
| PWA Support        | No      | Website that looks, feels and behaves like a native mobile app, overkill                                                                                                     |
| Router             | Yes     | Navigation between different pages without multiple server requests. Probably good for our different views (List, Map, Reservations)                                         |
| Vuex               | No      | State management not necessary in frontend, vuex deprecated in favour of pinia                                                                                               |
| CSS Pre-processors | Yes     | Not necessary on it's own but quasar depends on SASS so we might as well use it                                                                                              |
| Linter/Formatter   | Yes     | Requires good code style. I'd prefer having a strict linter for me to learn good style code, can be disabled int .eslintrc.jsc                                               |
| Unit Testing       | No      | Not much logic is happening in the frontend, so we probably don't need unit tests                                                                                            |
| E2E Testing        | No      | See above                                                                                                                                                                    |

| Configuration                | Answer                    | Additional Info                                                                                                                        |
| ---------------------------- | ------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| Vue.js version               | 3.x                       | Newest Version, recommended by vue, provides the new Composition api                                                                   |
| class-style component syntax | No                        | Lets you define Components like classes, could be interesting but we'd have to learn another (probably not that well supported) syntax |
| History mode for Router      | Yes                       | Backwards and forwards navigation within the website (without leaving the site), makes the app more intuitive                          |
| Css pre-processor            | Sass/SCSS                 | SASS is already required by Quasar, we might as well enable integration, purely optional|
| Linter/formatter config      | ESLint + Prettier         | The most used ESLint configuration (see [npm trends](https://www.npmtrends.com/eslint-config-airbnb-vs-prettier-vs-standard)                                                                                                                                     |
| When to lint                 | Lint on save              | Only linting on build/push seems like a bad error-prone idea                                                                                                                                       |
| Config file location         | In dedicated config files | Clutters the main directory but makes config files more obvious                                                                                                                                       |

| Additional Libraries         | Purpose                                         |
| ---------------------------- | ----------------------------------------------- |
| [Quasar](https://quasar.dev) | UI-Library, Material Components, Stable in Vue3 |
| date-fns                     | Datetime library                                 |
| TypeDoc                      | Documentation                                   |

---

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).