import { Environment } from "@/Environment";
import type { OpeningHours } from "@/models/OpeningHours";
import type { Timeslot } from "@/models/Timeslot";
import { format, eachMinuteOfInterval, addHours, addDays, max } from "date-fns";
import type Ref from "vue";
import { ref, watchEffect } from "vue";

export function tomorrowAsString(): string {
  return dateToString(addDays(new Date(), 1));
}

export function dateToString(date: Date): string {
  return format(date, "yyyy-MM-dd");
}

export function ISOStringToDateString(iso: string): string {
  return format(new Date(iso), "dd.MM.yyyy");
}

export function ISOStringToTimeString(iso: string): string {
  return format(new Date(iso), "HH:mm");
}

export function roundTimeDown(date: Date): Date {
  return new Date(date.getTime() - (date.getTime() % (1000 * 60 * 30)));
}

export function roundTimeUp(date: Date): Date {
  return new Date(roundTimeDown(date).getTime() + 1000 * 60 * 30);
}

export const parties: number[] = [];
for (let i = 1; i <= 20; i++) {
  parties.push(i);
}

export function nowAsString() {
  return format(roundTimeUp(new Date()), "HH:mm");
}

export const times = eachMinuteOfInterval(
  {
    start: new Date(2000, 1, 1, 0, 0),
    end: new Date(2000, 1, 1, 23, 59),
  },
  { step: 30 }
).map((value) => format(value, "HH:mm"));

export function useTimesInOpeningHours(
  ISOdate: Ref.Ref<string>,
  hours: Array<OpeningHours>
) {
  const availableTimes: Ref.Ref<Array<string>> = ref([]);

  function getTimes() {
    // "i" parses for day of week number starting with monday (1-7)
    const day = Number.parseInt(format(new Date(ISOdate.value), "i")) - 1;
    const start = max([
      roundTimeUp(new Date(ISOdate.value + "T" + hours[day].openingTime)),
      roundTimeUp(addHours(new Date(), 12)),
    ]);
    const end = roundTimeDown(
      new Date(ISOdate.value + "T" + hours[day].closingTime)
    );
    if (start >= end) {
      availableTimes.value = [];
    } else {
      availableTimes.value = eachMinuteOfInterval(
        { start: start, end: end },
        { step: 30 }
      ).map((value) => format(value, "HH:mm"));
    }
  }

  watchEffect(getTimes);

  return availableTimes;
}

export function dateValid(date: string) {
  return new Date(date) > new Date();
}

export enum TableType {
  WALL = "WALL",
  AISLE = "AISLE",
  WINDOW = "WINDOW",
  OUTSIDE = "OUTSIDE",
}

export function useGetTimeslots(
  restaurantId: string,
  date: Ref.Ref<string>,
  time: Ref.Ref<string>,
  party: Ref.Ref<number>,
  tableTypes: Ref.Ref<Array<TableType>>
) {
  const timeslots: Ref.Ref<Array<Timeslot>> = ref([]);
  const error = ref("");

  function doFetch() {
    error.value = "";

    const timeString = date.value + "T" + time.value + ":00";
    const sourceUrl = new URL(
      "restaurant/" + restaurantId + "/table",
      Environment.baseUrl
    );

    sourceUrl.searchParams.append("time", timeString);
    sourceUrl.searchParams.append("noOfPeople", party.value.toString());
    tableTypes.value.forEach((type) =>
      sourceUrl.searchParams.append("tableType", type)
    );

    fetch(sourceUrl.href)
      .then((res) => {
        if (res.ok) {
          res.json().then((json) => (timeslots.value = json));
        } else {
          res.json().then((json) => {
            error.value = json;
            timeslots.value = [];
          });
        }
      })
      .catch((err) => (error.value = err));
  }

  watchEffect(doFetch);

  return { timeslots, error };
}
