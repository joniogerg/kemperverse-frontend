import { Environment } from "@/Environment";
import type Ref from "vue";
import { ref } from "vue";

export function useRestaurantDetail(restaurantId: string) {
  const sourceUrl = new URL("restaurant/" + restaurantId, Environment.baseUrl);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const info: Ref.Ref<any> = ref(undefined);
  const error = ref("");

  fetch(sourceUrl.href)
    .then((res) => {
      if (res.ok) {
        res.json().then((json) => (info.value = json));
      } else {
        res.json().then((json) => (error.value = json));
      }
    })
    .catch((err) => (error.value = err));

  return { info, error };
}

export function useRestaurantImage(restaurantId: string) {
  const sourceUrl = new URL(
    "restaurant/" + restaurantId + "/image",
    Environment.baseUrl
  );
  const imageUrl = ref("");
  const error = ref("");

  fetch(sourceUrl.href)
    .then((res) => {
      if (res.ok) {
        res.blob().then((blob) => (imageUrl.value = URL.createObjectURL(blob)));
      } else {
        res.json().then((json) => (error.value = json));
      }
    })
    .catch((err) => (error.value = err));

  return { imageUrl, error };
}

export function getPriceAsNumber(priceDescription: string): number {
  switch (priceDescription) {
    case "LOW":
      return 1;
    case "MEDIUM":
      return 2;
    case "HIGH":
      return 3;
    default:
      return 0;
  }
}
