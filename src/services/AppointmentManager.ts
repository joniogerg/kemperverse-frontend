import { ref } from "vue";
import { Environment } from "@/Environment";

export function useCancelAppointment(id: string, token: string) {
  const message = ref("");
  const working = ref(false);

  function cancel() {
    const url = new URL(Environment.baseUrl + "/appointment/cancel/");
    url.searchParams.append("id", id);
    url.searchParams.append("token", token);
    working.value = true;
    fetch(url.href).then((res) => {
      if (res.ok) {
        message.value =
          "Appointment was successfully cancelled.\nYou can now leave this site.";
        working.value = false;
      } else {
        res.json().then((json) => {
          console.log(json);
          message.value =
            "Appointment could not be cancelled because:\n" +
            json.reason +
            "\nPlease try again or contact Kemperverse Support.";
          working.value = false;
        });
      }
    });
  }

  return { message, working, cancel };
}

export function useConfirmAppointment(id: string, token: string) {
  const message = ref("");
  const working = ref(false);

  function confirm() {
    working.value = true;

    const url = new URL(Environment.baseUrl + "/appointment/confirm");
    url.searchParams.append("id", id);
    url.searchParams.append("token", token);

    fetch(url.href).then((res) => {
      if (res.ok) {
        message.value =
          "Appointment was successfully confirmed.\nYou can now leave this site.";
        working.value = false;
      } else {
        res.json().then((json) => {
          console.log(json);
          message.value =
            "Appointment could not be confirmed because:\n" +
            json.reason +
            "\nPlease try again or contact Kemperverse Support.";
          working.value = false;
        });
      }
    });
  }

  return { message, working, confirm };
}
