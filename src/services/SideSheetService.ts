import { ref } from "vue";

export namespace SideSheetService {
  export const leftSheetOpen = ref(false);
  export const rightSheetOpen = ref(false);
}
