import { RestaurantType } from "@/models/RestaurantType";

export namespace Language {
  export function restaurantType(type: RestaurantType): string {
    switch (type) {
      case RestaurantType.GERMAN:
        return "German";
      case RestaurantType.ITALIAN:
        return "Italian";
      default: return type.toLowerCase().replace(/^./, type.charAt(0).toUpperCase())
    }
  }
}
