import type {Restaurant} from "@/models/Restaurant";
import {Environment} from "@/Environment";
import {RestaurantType} from "@/models/RestaurantType";
import {RestaurantPriceLevel} from "@/models/RestaurantPriceLevel";

// @ts-ignore
import _ from "lodash";
import type {PagedResponse} from "@/models/PagedResponse";
import type {PagingOptions} from "@/utils/PagingOptions";
import type {RestaurantSearchFilter} from "@/models/RestaurantSearchFilter";

export namespace RestaurantService {

    export function queryRestaurantList(
        filter?: RestaurantSearchFilter,
        page = 0
    ): Promise<PagedResponse<Restaurant>> {
        const opt = {
            page,
            size: 50,
            sort: [],
        };
        let url = Environment.baseUrl + "/restaurant/";
        if (filter) {
            let queries = Object.keys(filter)  // @ts-ignore
                .filter(k => filter[k] != null)
                .map<{k: string, v: any}>((k: string) => {
                    if (k === "openToday" && filter.openToday) {
                        let tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                        let localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1);
                        return { k: "openAt", v: localISOTime}
                    } else if(k === "distance" && filter.distance){
                        switch (filter.distance){
                            case 0: return {k, v: 1};
                            case 1: return {k, v: 3};
                            case 2: return {k, v: 5};
                            case 3: return {k, v: 10};
                            case 4: return {k, v: 20};
                            case 5: return {k, v: 50};
                            default: return {k, v: filter.distance}
                        }
                    }
                    return {k: k, v: (filter as any)[k]};
                })
            if(queries.find(val => val.k === "address" && val.v.trim().length != 0)){
                queries = queries.filter(val => (val.k !== "latitude" && val.k !== "longitude"))
            }
            url += "search?" + queries.map(e => {
                    let part = e.k + "=";
                    if (e.k === "restaurantType" && e.v) {
                        return part + Object.keys(e.v)
                            .filter(k => e.v[k])
                            .join("&" + e.k + "=")
                    } else {
                        return part + e.v;
                    }
                })
                .join("&")
        }
        if (opt.sort.length > 0) {
            url += `&sort=` + opt.sort.join(",");
        }
        url += "&page=" + opt.page + "&size=" + opt.size
        return fetch(url)
            .then((res) => res.json())
            .then((res: PagedResponse<any>) => {
                res.content.map((r: any) => {
                    return r as Restaurant;
                });
                return res;
            });
    }

}
