import type { Rating } from "@/models/Rating";
import type Ref from "vue";
import { ref } from "vue";
import { Environment } from "@/Environment";

export function useRatings(id: string) {
  const ratings: Ref.Ref<Rating[]> = ref([]);

  const url = Environment.baseUrl + "/restaurant/" + id + "/ratings";

  fetch(url).then((res) => {
    if (res.ok) {
      res.json().then((json) => (ratings.value = json));
    }
  });

  return ratings;
}
