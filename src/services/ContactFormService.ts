import { Environment } from "@/Environment";
import type Ref from "vue";
import { ref } from "vue";

export function useReserve(
  fullName: Ref.Ref<string>,
  email: Ref.Ref<string>,
  isoTime: string,
  party: number,
  restaurantId: string,
  tableId: number
) {
  const url = Environment.baseUrl + "/appointment/new";
  const headers = {
    "Content-Type": "application/json",
  };

  const message = ref("");
  const working = ref(false);
  const success = ref(false);

  function tryReserve() {
    working.value = true;
    const body = {
      fullName: fullName.value,
      email: email.value,
      startDate: isoTime,
      numberOfPersons: party.toString(),
      restaurantId: restaurantId,
      restaurantTable: tableId.toString(),
    };

    fetch(url, {
      method: "PUT",
      body: JSON.stringify(body),
      headers: headers,
    })
      .then((res) => {
        if (res.ok) {
          message.value = "Booked appointment successfully.";
          working.value = false;
          success.value = true;
        } else {
          res.json().then((json) => {
            message.value = json.reason;
            working.value = false;
            success.value = false;
          });
        }
      })
      .catch((err) => {
        console.log(err);
        working.value = false;
      });
  }

  return { tryReserve, working, success, message };
}
