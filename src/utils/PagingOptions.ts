export interface PagingOptions {
  page: number;
  size: number;
  sort: string[];
}
