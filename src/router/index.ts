import { createRouter, createWebHistory } from "vue-router";
import RestaurantDetailView from "@/views/RestaurantDetailView.vue";
import RestaurantListView from "@/views/RestaurantListView.vue";
import CancelAppointmentView from "@/views/CancelAppointmentView.vue";
import ConfirmAppointmentView from "@/views/ConfirmAppointmentView.vue";
import RestaurantMapView from "@/views/RestaurantMapView.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "restaurant-list",
      component: RestaurantListView,
    },
    {
      path: "/reserve/:restaurantId",
      name: "reserve",
      component: RestaurantDetailView,
      props: true,
    },
    {
      path: "/appointment/cancel",
      name: "cancel",
      component: CancelAppointmentView,
      props: (route) => ({
        id: route.query.id,
        token: route.query.token,
      }),
    },
    {
      path: "/appointment/confirm",
      name: "confirm",
      component: ConfirmAppointmentView,
      props: (route) => ({
        id: route.query.id,
        token: route.query.token,
      }),
    },
    {
      path: "/:catchAll(.*)",
      redirect: () => {
        return { name: "restaurant-list", query: {} };
      },
    },
  ],
});

export default router;
