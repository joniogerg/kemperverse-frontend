export enum RestaurantType {
  ITALIAN = "ITALIAN",
  GERMAN = "GERMAN",
  GREEK = "GREEK",
  CHINESE = "CHINESE",
  VIETNAMESE = "VIETNAMESE",
  KOREAN = "KOREAN",
  JAPAN = "JAPAN",
  ALBANIAN = "ALBANIAN",
  RUSSIAN = "RUSSIAN",
  CZECH = "CZECH",
  INDIAN = "INDIAN",
  MEXICAN = "MEXICAN",
  POLISH = "POLISH",
  THAI = "THAI",
  UKRAINIAN = "UKRAINIAN",
  TURKISH = "TURKISH"
}
