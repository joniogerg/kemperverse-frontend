export interface Rating {
  name: string;
  text: string;
  stars: number;
}
