import type { Calendar } from "@/models/Calendar";

export interface Table {
  noOfPlaces: number;
  type: string;
  calendar?: Calendar;
}
