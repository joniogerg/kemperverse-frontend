import type { TableType } from "@/services/TimeslotService";

export interface Timeslot {
  time: string;
  table: {
    tableNumber: number;
    type: TableType;
  };
}
