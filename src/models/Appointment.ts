export interface Appointment {
  startDate: string;
  endDate: string;
  noOfPersons: 1;
  id: string;
}
