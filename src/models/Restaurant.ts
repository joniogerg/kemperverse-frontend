import type { RestaurantType } from "@/models/RestaurantType";
import type { RestaurantPriceLevel } from "@/models/RestaurantPriceLevel";
import type { Coordinates } from "@/models/Coordinates";
import type { OpeningHours } from "@/models/OpeningHours";
import type { Table } from "@/models/Table";

export interface Restaurant {
  id: string;
  restaurantType: RestaurantType;
  priceLevel: RestaurantPriceLevel; // TODO: Request on backend side -> 1: Cheapest, 5: Most expensive
  name: string;
  phoneNumber: string;
  website: string;
  address: string;
  averageRating: number;
  ratings?: Array<any>; // TODO: Implement rating
  tables: Array<Table>;
  position: Coordinates;
  openingHours: Array<OpeningHours>;
}
