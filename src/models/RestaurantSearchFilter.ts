import type {RestaurantPriceLevel} from "@/models/RestaurantPriceLevel";
import {RestaurantType} from "@/models/RestaurantType";

export interface RestaurantSearchFilter{
    address?: string,
    name?: string,
    priceLevel?: RestaurantPriceLevel,
    ratingMin: number,
    distance?: number,
    openToday?: boolean,
    latitude?: number,
    longitude?: number,
    openAt?: String,
    restaurantType?: { [key in RestaurantType]?: boolean }
}

export function createRestaurantTypesTemplate(): { [key in RestaurantType]?: boolean }{
    let obj: any = {}
    for(let type in RestaurantType){
        obj[type] = false;
    }
    return obj
}
