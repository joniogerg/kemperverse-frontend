
export interface LocationModel {
    disabled: boolean,
    coords?: [number, number],
    distance: number,
}
