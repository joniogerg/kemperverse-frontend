import type { Appointment } from "@/models/Appointment";

export interface Calendar {
  appointments: Array<Appointment>;
}
